import vk
import re
import time
from urllib import request
import constants
import os
target_dir = 'photos'

os.makedirs(target_dir, exist_ok=True)

session = vk.Session(access_token=constants.token)
VK = vk.API(session)
api_v = '5.85'

lines = open('photos-with-faces.txt', 'r') .readlines()
total_count = len(lines)
pack_length = 100
packs = [lines[offset:offset+pack_length] for offset in range(0, total_count, pack_length)]
photo_packs = [",".join([re.search(r'] https://vk.com/photo(.+?) ', l).group(1) for l in pack]) for pack in packs]
first_response = VK.photos.getById(v=api_v, photos=photo_packs[0], photo_sizes=1)


def extract_links(pack):
    for i, photo in enumerate(pack):
        src = sorted(((s['width'], s['url']) for s in photo['sizes']), reverse=True)[0][1]
        request.urlretrieve(src, os.path.join(target_dir, src.rsplit('/', 1)[1]))
        print('Downloaded', i, 'of', len(pack))


extract_links(first_response)

for i in range(1, len(packs)):
    response = VK.photos.getById(v=api_v, photos=photo_packs[i], photo_sizes=1)
    extract_links(response)
    print('Requested', i * pack_length, 'of', total_count)
    time.sleep(0.5)
