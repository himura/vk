#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import vk
import time
import pickle
import constants
# https://oauth.vk.com/authorize?v=5.95&response_type=token&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope=friends,groups,photos&client_id=[Application_ID]
# https://vk.com/dev/utils.resolveScreenName

api_v = '5.95'
size = 'y'  # https://vk.com/dev/photo_sizes
links = []


def extract_links(pack):
    global links
    for photo in pack:
        photo_id, album = photo['id'], photo['album_id']
        photo_sizes = {s['type']: s['url'] for s in photo['sizes']}
        src = photo_sizes[size] if size in photo_sizes.keys() else photo_sizes[chr(ord(size)-1)]
        links.append({'id': photo_id, 'album': album, 'src': src})


# https://oauth.vk.com/authorize?v=5.85&response_type=token&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope=friends,groups,photos&client_id=[Application_ID]
session = vk.Session(access_token=constants.token)
VK = vk.API(session)

response = VK.photos.getAll(v=api_v, owner_id=constants.owner)
extract_links(response['items'])
total_count, pack_length = response['count'], len(response['items'])

for offset in range(pack_length, total_count, pack_length):
    extract_links(VK.photos.getAll(v=api_v, owner_id=constants.owner, offset=offset)['items'])
    print('Got', offset, 'of', total_count)
    time.sleep(0.5)
    break  # remove only when you are really ready to download everything


pickle.dump(links, open("links.p", "wb"))

print('жопиздан.\nActual count:', total_count, '\n      Dumped:', len(links))
