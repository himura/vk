#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime
from functools import partial
from urllib import request
import face_recognition
import multiprocessing
import numpy as np
import constants
import PIL.Image
import pickle
import io

accurate_search = True


def process_photo(lock, d):
    i, photo = d
    img = PIL.Image.open(io.BytesIO(request.urlopen(photo['src']).read()))
    img_array = np.array(img.convert('RGB'))
    face_locations = face_recognition.face_locations(img_array, model=("cnn" if accurate_search else "hog"))
    if len(face_locations) > 0:
        link = "https://vk.com/photo%s_%s" % (constants.owner, photo['id'])
        time = datetime.now().strftime('%H:%M:%S')
        line = '%4.0d [%s] %s - %d faces' % (i, time, link, len(face_locations))
        lock.acquire()
        print(line)
        with open('photos-with-faces.txt', 'a') as f:
            f.write(line + '\n')
        lock.release()


if __name__ == '__main__':
    items = enumerate(pickle.load(open("links.p", "rb")))
    pool = multiprocessing.Pool(4)
    manager = multiprocessing.Manager()
    lock = manager.Lock()
    pool.map(partial(process_photo, lock), items)
    pool.close()
    pool.join()
